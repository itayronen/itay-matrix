import { Matrix3 } from "./Matrix3";

// Tested by Matrix4 rotations.

export function setToRotationX(target: Matrix3, radians: number): void {
	let cos = Math.cos(radians);
	let sin = Math.sin(radians);

	target.m11 = 1; target.m12 = 0; target.m13 = 0;
	target.m21 = 0; target.m22 = cos; target.m23 = -sin;
	target.m31 = 0; target.m32 = sin; target.m33 = cos;
}

export function setToRotationY(target: Matrix3, radians: number): void {
	let cos = Math.cos(radians);
	let sin = Math.sin(radians);

	target.m11 = cos; target.m12 = 0; target.m13 = sin;
	target.m21 = 0; target.m22 = 1; target.m23 = 0;
	target.m31 = -sin; target.m32 = 0; target.m33 = cos;
}

export function setToRotationZ(target: Matrix3, radians: number): void {
	let cos = Math.cos(radians);
	let sin = Math.sin(radians);

	target.m11 = cos; target.m12 = -sin; target.m13 = 0;
	target.m21 = sin; target.m22 = cos; target.m23 = 0;
	target.m31 = 0; target.m32 = 0; target.m33 = 1;
}

/**
 *  Intrinsic order. From world system perspective, will be applied in reverse order.
 */
export function setToRotationXYZ(target: Matrix3, radiansX: number, radiansY: number, radiansZ: number): void {
	let cosX = Math.cos(radiansX);
	let sinX = Math.sin(radiansX);
	let cosY = Math.cos(radiansY);
	let sinY = Math.sin(radiansY);
	let cosZ = Math.cos(radiansZ);
	let sinZ = Math.sin(radiansZ);

	let cosXsinZ = cosX * sinZ;
	let cosZsinX = cosZ * sinX;
	let cosXcosZ = cosX * cosZ;
	let sinXsinZ = sinX * sinZ;

	target.m11 = cosY * cosZ;
	target.m12 = -cosY * sinZ;
	target.m13 = sinY;

	target.m21 = cosXsinZ + (cosZsinX * sinY);
	target.m22 = cosXcosZ - (sinXsinZ * sinY);
	target.m23 = -cosY * sinX;

	target.m31 = sinXsinZ - (cosXcosZ * sinY);
	target.m32 = cosZsinX + (cosXsinZ * sinY);
	target.m33 = cosX * cosY;
}

/**
 *  Intrinsic order. From world system perspective, will be applied in reverse order.
 */
export function setToRotationYXZ(target: Matrix3, radiansY: number, radiansX: number, radiansZ: number): void {
	let cosX = Math.cos(radiansX);
	let sinX = Math.sin(radiansX);
	let cosY = Math.cos(radiansY);
	let sinY = Math.sin(radiansY);
	let cosZ = Math.cos(radiansZ);
	let sinZ = Math.sin(radiansZ);

	let cosYcosZ = cosY * cosZ;
	let cosYsinZ = cosY * sinZ;
	let cosZsinY = cosZ * sinY;
	let sinYsinZ = sinY * sinZ;

	target.m11 = cosYcosZ + sinYsinZ * sinX;
	target.m12 = cosZsinY * sinX - cosYsinZ;
	target.m13 = cosX * sinY;

	target.m21 = cosX * sinZ;
	target.m22 = cosX * cosZ;
	target.m23 = -sinX;

	target.m31 = cosYsinZ * sinX - cosZsinY;
	target.m32 = sinYsinZ + cosYcosZ * sinX;
	target.m33 = cosX * cosY;
}