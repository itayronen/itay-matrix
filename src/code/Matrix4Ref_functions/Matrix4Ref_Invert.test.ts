import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";
import { invert, invertAffine } from "./Matrix4Ref_Invert";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref", suite => {
		suite.describe("invert", suite => {
			suite.test("When invertible matrix, then target is set to inverse and returns true.", t => {
				t.arrange();
				let startMatrix = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);
				let matrix = Matrix4.createClone(startMatrix);

				t.act();
				let result = Matrix4.createIdentity();
				let invertible = invert(result, matrix);

				t.assert();
				let expected = Matrix4.create(
					-1, 2, -1, 0,
					1.625, -4, -0.625, 2,
					-1.75, 2, 4.75, -4,
					1, 0, -3, 2
				);
				expect(Matrix4.areEquals(result, expected)).to.be.true;
				expect(invertible).to.be.true;
				expect(Matrix4.areEquals(matrix, startMatrix)).to.be.true;
			});

			suite.test("When invertible matrix as input and output, then set to inverse and returns true.", t => {
				t.arrange();
				let matrix = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);

				t.act();
				let invertible = invert(matrix, matrix);

				t.assert();
				let expected = Matrix4.create(
					-1, 2, -1, 0,
					1.625, -4, -0.625, 2,
					-1.75, 2, 4.75, -4,
					1, 0, -3, 2
				);
				expect(Matrix4.areEquals(matrix, expected));
				expect(invertible).to.be.true;
			});

			suite.test("When not-invertible matrix, then target is set to identity and returns false.", t => {
				t.arrange();
				let startMatrix = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
				let matrix = Matrix4.createClone(startMatrix);

				t.act();
				let result = Matrix4.create(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
				let invertible = invert(result, matrix);

				t.assert();
				expect(Matrix4.areEquals(result, Matrix4.createIdentity())).to.be.true;
				expect(invertible).to.be.false;
				expect(Matrix4.areEquals(matrix, startMatrix)).to.be.true;
			});
		});

		suite.describe("invertAffine", suite => {
			suite.test("Target is set to inverse and input is untouched.", t => {
				t.arrange();
				let startMatrix = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 0, 0, 0, 1);
				let matrix = Matrix4.createClone(startMatrix);

				t.act();
				let result = Matrix4.createIdentity();
				let invertible = invertAffine(result, matrix);

				t.assert();
				let expected = Matrix4.create(
					-1, 2, -1, 0,
					0.625, -4, 2.375, 1,
					0.25, 2, -1.25, -2,
					0, 0, 0, 1
				);
				expect(Matrix4.areCloseToEquals(result, expected)).to.be.true;
				expect(Matrix4.areEquals(matrix, startMatrix)).to.be.true;
			});
		});
	});
}