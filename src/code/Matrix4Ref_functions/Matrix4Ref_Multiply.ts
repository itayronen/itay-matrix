import { Matrix4 } from './Matrix4';

export function multiply(target: Matrix4, a: Matrix4, b: Matrix4): void {
	let m11 = (a.m11 * b.m11) + (a.m12 * b.m21) + (a.m13 * b.m31) + (a.m14 * b.m41);
	let m12 = (a.m11 * b.m12) + (a.m12 * b.m22) + (a.m13 * b.m32) + (a.m14 * b.m42);
	let m13 = (a.m11 * b.m13) + (a.m12 * b.m23) + (a.m13 * b.m33) + (a.m14 * b.m43);
	let m14 = (a.m11 * b.m14) + (a.m12 * b.m24) + (a.m13 * b.m34) + (a.m14 * b.m44);

	let m21 = (a.m21 * b.m11) + (a.m22 * b.m21) + (a.m23 * b.m31) + (a.m24 * b.m41);
	let m22 = (a.m21 * b.m12) + (a.m22 * b.m22) + (a.m23 * b.m32) + (a.m24 * b.m42);
	let m23 = (a.m21 * b.m13) + (a.m22 * b.m23) + (a.m23 * b.m33) + (a.m24 * b.m43);
	let m24 = (a.m21 * b.m14) + (a.m22 * b.m24) + (a.m23 * b.m34) + (a.m24 * b.m44);

	let m31 = (a.m31 * b.m11) + (a.m32 * b.m21) + (a.m33 * b.m31) + (a.m34 * b.m41);
	let m32 = (a.m31 * b.m12) + (a.m32 * b.m22) + (a.m33 * b.m32) + (a.m34 * b.m42);
	let m33 = (a.m31 * b.m13) + (a.m32 * b.m23) + (a.m33 * b.m33) + (a.m34 * b.m43);
	let m34 = (a.m31 * b.m14) + (a.m32 * b.m24) + (a.m33 * b.m34) + (a.m34 * b.m44);

	let m41 = (a.m41 * b.m11) + (a.m42 * b.m21) + (a.m43 * b.m31) + (a.m44 * b.m41);
	let m42 = (a.m41 * b.m12) + (a.m42 * b.m22) + (a.m43 * b.m32) + (a.m44 * b.m42);
	let m43 = (a.m41 * b.m13) + (a.m42 * b.m23) + (a.m43 * b.m33) + (a.m44 * b.m43);
	let m44 = (a.m41 * b.m14) + (a.m42 * b.m24) + (a.m43 * b.m34) + (a.m44 * b.m44);

	target.m11 = m11; target.m12 = m12; target.m13 = m13; target.m14 = m14;
	target.m21 = m21; target.m22 = m22; target.m23 = m23; target.m24 = m24;
	target.m31 = m31; target.m32 = m32; target.m33 = m33; target.m34 = m34;
	target.m41 = m41; target.m42 = m42; target.m43 = m43; target.m44 = m44;
}

export function multiplyScalar(target: Matrix4, scalar: number): void {
	target.m11 *= scalar; target.m12 *= scalar; target.m13 *= scalar; target.m14 *= scalar;
	target.m21 *= scalar; target.m22 *= scalar; target.m23 *= scalar; target.m24 *= scalar;
	target.m31 *= scalar; target.m32 *= scalar; target.m33 *= scalar; target.m34 *= scalar;
	target.m41 *= scalar; target.m42 *= scalar; target.m43 *= scalar; target.m44 *= scalar;
}