import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";
import { setToIdentity } from "./Matrix4Ref_SetToIdentity";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref_SetToIdentity", suite => {
		suite.test("setToIdentity", t => {
			t.arrange();
			let matrix = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

			t.act();
			setToIdentity(matrix);

			t.assert();
			expect(Matrix4.areEquals(matrix, Matrix4.createIdentity())).to.be.true;
		});
	});
}