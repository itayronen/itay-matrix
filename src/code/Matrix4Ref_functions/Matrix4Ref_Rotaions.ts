import { Matrix4 } from './Matrix4';
import * as Matrix3Ref from "./Matrix3Ref";

export function setToRotationX(target: Matrix4, radians: number): void {
	Matrix3Ref.setToRotationX(target, radians);

	target.m14 = 0;
	target.m24 = 0;
	target.m34 = 0;

	target.m41 = 0;
	target.m42 = 0;
	target.m43 = 0;
	target.m44 = 1;
}

export function setToRotationY(target: Matrix4, radians: number): void {
	Matrix3Ref.setToRotationY(target, radians);

	target.m14 = 0;
	target.m24 = 0;
	target.m34 = 0;

	target.m41 = 0;
	target.m42 = 0;
	target.m43 = 0;
	target.m44 = 1;
}

export function setToRotationZ(target: Matrix4, radians: number): void {
	Matrix3Ref.setToRotationZ(target, radians);

	target.m14 = 0;
	target.m24 = 0;
	target.m34 = 0;

	target.m41 = 0;
	target.m42 = 0;
	target.m43 = 0;
	target.m44 = 1;
}

/**
 *  Intrinsic order. From world system perspective, will be applied in reverse order.
 */
export function setToRotationXYZ(target: Matrix4, radiansX: number, radiansY: number, radiansZ: number): void {
	Matrix3Ref.setToRotationXYZ(target, radiansX, radiansY, radiansZ);

	target.m14 = 0;
	target.m24 = 0;
	target.m34 = 0;

	target.m41 = 0;
	target.m42 = 0;
	target.m43 = 0;
	target.m44 = 1;
}

/**
 *  Intrinsic order. From world system perspective, will be applied in reverse order.
 */
export function setToRotationYXZ(target: Matrix4, radiansY: number, radiansX: number, radiansZ: number): void {
	Matrix3Ref.setToRotationYXZ(target, radiansY, radiansX, radiansZ);

	target.m14 = 0;
	target.m24 = 0;
	target.m34 = 0;

	target.m41 = 0;
	target.m42 = 0;
	target.m43 = 0;
	target.m44 = 1;
}