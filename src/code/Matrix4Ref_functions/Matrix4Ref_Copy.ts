import { Matrix4 } from './Matrix4';

export function copy(target: Matrix4, m:Matrix4): void {
	target.m11 = m.m11; target.m12 = m.m12; target.m13 = m.m13; target.m14 = m.m14;
	target.m21 = m.m21; target.m22 = m.m22; target.m23 = m.m23; target.m24 = m.m24;
	target.m31 = m.m31; target.m32 = m.m32; target.m33 = m.m33; target.m34 = m.m34;
	target.m41 = m.m41; target.m42 = m.m42; target.m43 = m.m43; target.m44 = m.m44;
}