import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";
import { applyOrder } from "./Matrix4Ref";
import { setToScale, scale } from "./Matrix4Ref_Scale";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref_Scale", suite => {
		suite.test("setToScale", t => {
			t.arrange();
			let matrix = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

			t.act();
			setToScale(matrix, 1, 2, 3);

			t.assert();
			expect(Matrix4.areEquals(matrix, Matrix4.createScale(1, 2, 3))).to.be.true;
		});

		suite.test("scale", t => {
			t.arrange();
			let startMatrix = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
			let matrix = Matrix4.createClone(startMatrix);

			t.act();
			scale(matrix, matrix, 1, 2, 3);

			t.assert();
			let expected = Matrix4.createIdentity();
			applyOrder(expected, startMatrix, Matrix4.createScale(1, 2, 3));

			expect(Matrix4.areEquals(matrix, expected)).to.be.true;
		});
	});
}