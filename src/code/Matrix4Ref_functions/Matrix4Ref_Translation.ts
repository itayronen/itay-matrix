import { Matrix4 } from './Matrix4';

export function setToTranslation(target: Matrix4, x: number, y: number, z: number): void {
	target.m11 = 1; target.m12 = 0; target.m13 = 0; target.m14 = x;
	target.m21 = 0; target.m22 = 1; target.m23 = 0; target.m24 = y;
	target.m31 = 0; target.m32 = 0; target.m33 = 1; target.m34 = z;
	target.m41 = 0; target.m42 = 0; target.m43 = 0; target.m44 = 1;
}

export function translate(out: Matrix4, m: Matrix4, x: number, y: number, z: number): void {
	out.m11 = m.m11 + m.m41 * x; out.m12 = m.m12 + m.m42 * x; out.m13 = m.m13 + m.m43 * x; out.m14 = m.m14 + m.m44 * x;
	out.m21 = m.m21 + m.m41 * y; out.m22 = m.m22 + m.m42 * y; out.m23 = m.m23 + m.m43 * y; out.m24 = m.m24 + m.m44 * y;
	out.m31 = m.m31 + m.m41 * z; out.m32 = m.m32 + m.m42 * z; out.m33 = m.m33 + m.m43 * z; out.m34 = m.m34 + m.m44 * z;
}