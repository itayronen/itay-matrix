import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";
import { applyOrder } from "./Matrix4Ref";
import {
	setToRotationX, setToRotationY, setToRotationZ, setToRotationXYZ, setToRotationYXZ
} from "./Matrix4Ref_Rotaions";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref", suite => {
		suite.test("setRotationX.", t => {
			t.arrange();
			let matrix = Matrix4.createIdentity();

			t.act();
			setToRotationX(matrix, 1);

			t.assert();
			let expected = Matrix4.createRotationX(1);
			expect(Matrix4.areEquals(matrix, expected)).to.be.true;
		});

		suite.test("setRotationY.", t => {
			t.arrange();
			let matrix = Matrix4.createIdentity();

			t.act();
			setToRotationY(matrix, 1);

			t.assert();
			let expected = Matrix4.createRotationY(1);
			expect(Matrix4.areEquals(matrix, expected)).to.be.true;
		});
		
		suite.test("setRotationZ.", t => {
			t.arrange();
			let matrix = Matrix4.createIdentity();

			t.act();
			setToRotationZ(matrix, 1);

			t.assert();
			let expected = Matrix4.createRotationZ(1);
			expect(Matrix4.areEquals(matrix, expected)).to.be.true;
		});

		suite.test("setRotationXYZ.", t => {
			t.arrange();
			let matrix = Matrix4.createIdentity();

			t.act();
			setToRotationXYZ(matrix, 1, 2, 3);

			t.assert();
			let expected = Matrix4.createIdentity();
			applyOrder(expected,
				Matrix4.createRotationZ(3),
				Matrix4.createRotationY(2),
				Matrix4.createRotationX(1),
			);

			expect(Matrix4.areCloseToEquals(matrix, expected)).to.be.true;
		});
		
		suite.test("setRotationYXZ.", t => {
			t.arrange();
			let matrix = Matrix4.createIdentity();

			t.act();
			setToRotationYXZ(matrix, 1, 2, 3);

			t.assert();
			let expected = Matrix4.createIdentity();
			applyOrder(expected,
				Matrix4.createRotationZ(3),
				Matrix4.createRotationX(2),
				Matrix4.createRotationY(1),
			);

			expect(Matrix4.areCloseToEquals(matrix, expected)).to.be.true;
		});
	});
}