import { Matrix4 } from './Matrix4';

/**
 * Sets the target matrix to the values in the given array, assumes column by column.
 */
export function setFromArray(target: Matrix4, arr: ArrayLike<number>, from = 0): void {
	if (from === 0) {
		target.m11 = arr[0]; target.m12 = arr[4]; target.m13 = arr[8]; target.m14 = arr[12];
		target.m21 = arr[1]; target.m22 = arr[5]; target.m23 = arr[9]; target.m24 = arr[13];
		target.m31 = arr[2]; target.m32 = arr[6]; target.m33 = arr[10]; target.m34 = arr[14];
		target.m41 = arr[3]; target.m42 = arr[7]; target.m43 = arr[11]; target.m44 = arr[15];
	}
	else {
		target.m11 = arr[from++];
		target.m21 = arr[from++];
		target.m31 = arr[from++];
		target.m41 = arr[from++];
		target.m12 = arr[from++];
		target.m22 = arr[from++];
		target.m32 = arr[from++];
		target.m42 = arr[from++];
		target.m13 = arr[from++];
		target.m23 = arr[from++];
		target.m33 = arr[from++];
		target.m43 = arr[from++];
		target.m14 = arr[from++];
		target.m24 = arr[from++];
		target.m34 = arr[from++];
		target.m44 = arr[from];
	}
}

/**
 * Sets the target array to the values of the given matrix, column by column.
 */
export function toArray(target: number[], matrix: Readonly<Matrix4>, from = 0): void {
	if (from === 0) {
		target[0] = matrix.m11; target[4] = matrix.m12; target[8] = matrix.m13; target[12] = matrix.m14;
		target[1] = matrix.m21; target[5] = matrix.m22; target[9] = matrix.m23; target[13] = matrix.m24;
		target[2] = matrix.m31; target[6] = matrix.m32; target[10] = matrix.m33; target[14] = matrix.m34;
		target[3] = matrix.m41; target[7] = matrix.m42; target[11] = matrix.m43; target[15] = matrix.m44;
	}
	else {
		target[from++] = matrix.m11;
		target[from++] = matrix.m21;
		target[from++] = matrix.m31;
		target[from++] = matrix.m41;
		target[from++] = matrix.m12;
		target[from++] = matrix.m22;
		target[from++] = matrix.m32;
		target[from++] = matrix.m42;
		target[from++] = matrix.m13;
		target[from++] = matrix.m23;
		target[from++] = matrix.m33;
		target[from++] = matrix.m43;
		target[from++] = matrix.m14;
		target[from++] = matrix.m24;
		target[from++] = matrix.m34;
		target[from] = matrix.m44;
	}
}