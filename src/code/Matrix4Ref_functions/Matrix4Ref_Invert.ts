import { Matrix4 } from './Matrix4';
import * as Matrix3Ref from "./Matrix3Ref";
import { setToIdentity } from "./Matrix4Ref_SetToIdentity";

/**
 * Returns whether the the matrix is invertible, and the invert succeded.
 * When the invert fails, the matrix will be set to the identity matrix.
 */
export function invert(target: Matrix4, m: Matrix4): boolean {
	if (m.m41 === 0 && m.m42 === 0 && m.m43 === 0 && m.m44 === 1) {
		invertAffine(target, m);
		return true;
	}
	else {
		return invertGeneral(target, m);
	}
}

// http://www.songho.ca/opengl/gl_matrix.html
export function invertAffine(target: Matrix4, m: Matrix4): void {
	Matrix3Ref.invert(target, m);

	let x = m.m14;
	let y = m.m24;
	let z = m.m34;
	target.m14 = -(target.m11 * x + target.m12 * y + target.m13 * z);
	target.m24 = -(target.m21 * x + target.m22 * y + target.m23 * z);
	target.m34 = -(target.m31 * x + target.m32 * y + target.m33 * z);
}

export function invertGeneral(target: Matrix4, m: Matrix4): boolean {
	let a = m.m23 * m.m34 * m.m42 - m.m24 * m.m33 * m.m42 + m.m24 * m.m32 * m.m43 - m.m22 * m.m34 * m.m43 - m.m23 * m.m32 * m.m44 + m.m22 * m.m33 * m.m44;
	let b = m.m14 * m.m33 * m.m42 - m.m13 * m.m34 * m.m42 - m.m14 * m.m32 * m.m43 + m.m12 * m.m34 * m.m43 + m.m13 * m.m32 * m.m44 - m.m12 * m.m33 * m.m44;
	let c = m.m13 * m.m24 * m.m42 - m.m14 * m.m23 * m.m42 + m.m14 * m.m22 * m.m43 - m.m12 * m.m24 * m.m43 - m.m13 * m.m22 * m.m44 + m.m12 * m.m23 * m.m44;
	let d = m.m14 * m.m23 * m.m32 - m.m13 * m.m24 * m.m32 - m.m14 * m.m22 * m.m33 + m.m12 * m.m24 * m.m33 + m.m13 * m.m22 * m.m34 - m.m12 * m.m23 * m.m34;

	var determinant = m.m11 * a + m.m21 * b + m.m31 * c + m.m41 * d;

	if (determinant === 0) {
		setToIdentity(target);
		return false;
	}

	var inverseDeterminant = 1 / determinant;

	let m21 = m.m24 * m.m33 * m.m41 - m.m23 * m.m34 * m.m41 - m.m24 * m.m31 * m.m43 + m.m21 * m.m34 * m.m43 + m.m23 * m.m31 * m.m44 - m.m21 * m.m33 * m.m44;
	let m31 = m.m22 * m.m34 * m.m41 - m.m24 * m.m32 * m.m41 + m.m24 * m.m31 * m.m42 - m.m21 * m.m34 * m.m42 - m.m22 * m.m31 * m.m44 + m.m21 * m.m32 * m.m44;
	let m41 = m.m23 * m.m32 * m.m41 - m.m22 * m.m33 * m.m41 - m.m23 * m.m31 * m.m42 + m.m21 * m.m33 * m.m42 + m.m22 * m.m31 * m.m43 - m.m21 * m.m32 * m.m43;

	let m22 = m.m13 * m.m34 * m.m41 - m.m14 * m.m33 * m.m41 + m.m14 * m.m31 * m.m43 - m.m11 * m.m34 * m.m43 - m.m13 * m.m31 * m.m44 + m.m11 * m.m33 * m.m44;
	let m32 = m.m14 * m.m32 * m.m41 - m.m12 * m.m34 * m.m41 - m.m14 * m.m31 * m.m42 + m.m11 * m.m34 * m.m42 + m.m12 * m.m31 * m.m44 - m.m11 * m.m32 * m.m44;
	let m42 = m.m12 * m.m33 * m.m41 - m.m13 * m.m32 * m.m41 + m.m13 * m.m31 * m.m42 - m.m11 * m.m33 * m.m42 - m.m12 * m.m31 * m.m43 + m.m11 * m.m32 * m.m43;

	let m23 = m.m14 * m.m23 * m.m41 - m.m13 * m.m24 * m.m41 - m.m14 * m.m21 * m.m43 + m.m11 * m.m24 * m.m43 + m.m13 * m.m21 * m.m44 - m.m11 * m.m23 * m.m44;
	let m33 = m.m12 * m.m24 * m.m41 - m.m14 * m.m22 * m.m41 + m.m14 * m.m21 * m.m42 - m.m11 * m.m24 * m.m42 - m.m12 * m.m21 * m.m44 + m.m11 * m.m22 * m.m44;
	let m43 = m.m13 * m.m22 * m.m41 - m.m12 * m.m23 * m.m41 - m.m13 * m.m21 * m.m42 + m.m11 * m.m23 * m.m42 + m.m12 * m.m21 * m.m43 - m.m11 * m.m22 * m.m43;

	let m24 = m.m13 * m.m24 * m.m31 - m.m14 * m.m23 * m.m31 + m.m14 * m.m21 * m.m33 - m.m11 * m.m24 * m.m33 - m.m13 * m.m21 * m.m34 + m.m11 * m.m23 * m.m34;
	let m34 = m.m14 * m.m22 * m.m31 - m.m12 * m.m24 * m.m31 - m.m14 * m.m21 * m.m32 + m.m11 * m.m24 * m.m32 + m.m12 * m.m21 * m.m34 - m.m11 * m.m22 * m.m34;
	let m44 = m.m12 * m.m23 * m.m31 - m.m13 * m.m22 * m.m31 + m.m13 * m.m21 * m.m32 - m.m11 * m.m23 * m.m32 - m.m12 * m.m21 * m.m33 + m.m11 * m.m22 * m.m33;

	target.m11 = a * inverseDeterminant;
	target.m21 = m21 * inverseDeterminant;
	target.m31 = m31 * inverseDeterminant;
	target.m41 = m41 * inverseDeterminant;

	target.m12 = b * inverseDeterminant;
	target.m22 = m22 * inverseDeterminant;
	target.m32 = m32 * inverseDeterminant;
	target.m42 = m42 * inverseDeterminant;

	target.m13 = c * inverseDeterminant;
	target.m23 = m23 * inverseDeterminant;
	target.m33 = m33 * inverseDeterminant;
	target.m43 = m43 * inverseDeterminant;

	target.m14 = d * inverseDeterminant;
	target.m24 = m24 * inverseDeterminant;
	target.m34 = m34 * inverseDeterminant;
	target.m44 = m44 * inverseDeterminant;

	return true;
}