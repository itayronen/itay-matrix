import { Matrix4 } from './Matrix4';

export function setToScale(target: Matrix4, x: number, y: number, z: number): void {
	target.m11 = x; target.m12 = 0; target.m13 = 0; target.m14 = 0;
	target.m21 = 0; target.m22 = y; target.m23 = 0; target.m24 = 0;
	target.m31 = 0; target.m32 = 0; target.m33 = z; target.m34 = 0;
	target.m41 = 0; target.m42 = 0; target.m43 = 0; target.m44 = 1;
}

export function scale(target: Matrix4, m: Matrix4, x: number, y: number, z: number): void {
	target.m11 = m.m11 * x; target.m12 = m.m12 * x; target.m13 = m.m13 * x; target.m14 = m.m14 * x;
	target.m21 = m.m21 * y; target.m22 = m.m22 * y; target.m23 = m.m23 * y; target.m24 = m.m24 * y;
	target.m31 = m.m31 * z; target.m32 = m.m32 * z; target.m33 = m.m33 * z; target.m34 = m.m34 * z;
}