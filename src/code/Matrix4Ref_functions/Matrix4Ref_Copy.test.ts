import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";
import { copy } from "./Matrix4Ref_Copy";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref_Copy", suite => {
		suite.test("test.", t => {
			t.arrange();
			let startMatrix = Matrix4.create(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
			let matrix = Matrix4.createClone(startMatrix);

			t.act();
			let actual = Matrix4.createIdentity();
			let invertible = copy(actual, matrix);

			t.assert();
			expect(Matrix4.areEquals(actual, startMatrix)).to.be.true;
			expect(Matrix4.areEquals(matrix, startMatrix)).to.be.true;
		});
	});
}