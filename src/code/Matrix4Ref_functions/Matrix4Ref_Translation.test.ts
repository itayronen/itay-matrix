import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";
import { applyOrder } from "./Matrix4Ref";
import { setToTranslation, translate } from "./Matrix4Ref_Translation";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref_Translation", suite => {
		suite.test("setToTranslation", t => {
			t.arrange();
			let matrix = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

			t.act();
			setToTranslation(matrix, 1, 2, 3);

			t.assert();
			expect(Matrix4.areEquals(matrix, Matrix4.createTranslation(1, 2, 3))).to.be.true;
		});

		suite.test("translate", t => {
			t.arrange();
			let startMatrix = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
			let matrix = Matrix4.createClone(startMatrix);

			t.act();
			translate(matrix, matrix, 1, 2, 3);

			t.assert();
			let expected = Matrix4.createIdentity();
			applyOrder(expected, startMatrix, Matrix4.createTranslation(1, 2, 3));

			expect(Matrix4.areEquals(matrix, expected)).to.be.true;
		});
	});
}