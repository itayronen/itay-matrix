import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from './Matrix4';
import { applyOrder } from "./Matrix4Ref_ApplyOrder";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref_ApplyOrder", suite => {
		suite.test("Two matrices.", t => {
			t.arrange();
			let a = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);
			let b = Matrix4.create(2, 3, 4, 5, 6.5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17.5);

			t.act();
			let result = Matrix4.createIdentity();
			applyOrder(result, b, a);

			t.assert();
			let expected = Matrix4.create(
				101, 110, 120, 132,
				232, 255.5, 282, 312.5,
				361, 398, 440, 488,
				498, 549.5, 608, 674.75
			);
			expect(Matrix4.areEquals(result, expected)).to.be.true;
		});

		suite.test("Three matrices.", t => {
			t.arrange();
			let a = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);
			let b = Matrix4.create(2, 3, 4, 5, 6.5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17.5);

			t.act();
			let result = Matrix4.createIdentity();
			applyOrder(result, b, Matrix4.createIdentity(), a);

			t.assert();
			let expected = Matrix4.create(
				101, 110, 120, 132,
				232, 255.5, 282, 312.5,
				361, 398, 440, 488,
				498, 549.5, 608, 674.75
			);
			expect(Matrix4.areEquals(result, expected)).to.be.true;
		});

		suite.test("Four matrices.", t => {
			t.arrange();
			let a = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);
			let b = Matrix4.create(2, 3, 4, 5, 6.5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17.5);

			t.act();
			let result = Matrix4.createIdentity();
			applyOrder(result, Matrix4.createIdentity(), b, Matrix4.createIdentity(), a);

			t.assert();
			let expected = Matrix4.create(
				101, 110, 120, 132,
				232, 255.5, 282, 312.5,
				361, 398, 440, 488,
				498, 549.5, 608, 674.75
			);
			expect(Matrix4.areEquals(result, expected)).to.be.true;
		});
	});
}