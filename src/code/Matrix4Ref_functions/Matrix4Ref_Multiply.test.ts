import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from './Matrix4';
import { multiply, multiplyScalar } from "./Matrix4Ref_Multiply";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4Ref_Multiply", suite => {

		suite.describe("multiply", suite => {
			suite.test("Target is set to result and inputs are untouched.", t => {
				t.arrange();
				let startA = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);
				let startB = Matrix4.create(2, 3, 4, 5, 6.5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17.5);
				let a = Matrix4.createClone(startA);
				let b = Matrix4.createClone(startB);

				t.act();
				let result = Matrix4.createIdentity();
				multiply(result, a, b);

				t.assert();
				let expected = Matrix4.create(
					101, 110, 120, 132,
					232, 255.5, 282, 312.5,
					361, 398, 440, 488,
					498, 549.5, 608, 674.75
				);
				expect(Matrix4.areEquals(result, expected)).to.be.true;
				expect(Matrix4.areEquals(a, startA)).to.be.true;
				expect(Matrix4.areEquals(b, startB)).to.be.true;
			});

			suite.test("When target is also input, then its set to result and other input is untouched.", t => {
				t.arrange();
				let a = Matrix4.create(1, 2, 3, 4, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16.5);
				let startB = Matrix4.create(2, 3, 4, 5, 6.5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17.5);
				let b = Matrix4.createClone(startB);

				t.act();
				multiply(a, a, b);

				t.assert();
				let expected = Matrix4.create(
					101, 110, 120, 132,
					232, 255.5, 282, 312.5,
					361, 398, 440, 488,
					498, 549.5, 608, 674.75
				);
				expect(Matrix4.areEquals(a, expected)).to.be.true;
				expect(Matrix4.areEquals(b, startB)).to.be.true;
			});
		});
	});
}