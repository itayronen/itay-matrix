import { Matrix4 } from './Matrix4';

export function setToIdentity(target: Matrix4): void {
	target.m11 = 1; target.m12 = 0; target.m13 = 0; target.m14 = 0;
	target.m21 = 0; target.m22 = 1; target.m23 = 0; target.m24 = 0;
	target.m31 = 0; target.m32 = 0; target.m33 = 1; target.m34 = 0;
	target.m41 = 0; target.m42 = 0; target.m43 = 0; target.m44 = 1;
}