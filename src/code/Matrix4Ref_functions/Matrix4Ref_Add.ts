import { Matrix4 } from './Matrix4';

export function add(target: Matrix4, a: Matrix4, b: Matrix4): void {
	target.m11 = a.m11 + b.m11; target.m12 = a.m12 + b.m12; target.m13 = a.m13 + b.m13; target.m14 = a.m14 + b.m14;
	target.m21 = a.m21 + b.m21; target.m22 = a.m22 + b.m22; target.m23 = a.m23 + b.m23; target.m24 = a.m24 + b.m24;
	target.m31 = a.m31 + b.m31; target.m32 = a.m32 + b.m32; target.m33 = a.m33 + b.m33; target.m34 = a.m34 + b.m34;
	target.m41 = a.m41 + b.m41; target.m42 = a.m42 + b.m42; target.m43 = a.m43 + b.m43; target.m44 = a.m44 + b.m44;
}