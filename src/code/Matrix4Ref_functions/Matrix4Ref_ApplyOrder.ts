import { Matrix4 } from './Matrix4';
import { multiply } from "./Matrix4Ref_Multiply";

export function applyOrder(target: Matrix4, a: Matrix4, b: Matrix4): void
export function applyOrder(target: Matrix4, a: Matrix4, b: Matrix4, c: Matrix4): void
export function applyOrder(target: Matrix4, a: Matrix4, b: Matrix4, c: Matrix4, d: Matrix4): void
export function applyOrder(target: Matrix4, a: Matrix4, b: Matrix4, c?: Matrix4, d?: Matrix4): void {
	multiply(target, b, a);

	if (c) {
		multiply(target, c, target);

		if (d) {
			multiply(target, d, target);
		}
	}
}