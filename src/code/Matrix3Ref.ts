export * from "./Matrix3Ref_functions/Matrix3Ref_Rotations";

import { Matrix3 } from "./Matrix3";

export function setToIdentity(target: Matrix3): void {
	target.m11 = 1; target.m12 = 0; target.m13 = 0;
	target.m21 = 0; target.m22 = 1; target.m23 = 0;
	target.m31 = 0; target.m32 = 0; target.m33 = 1;
}

export function invert(target: Matrix3, m: Matrix3): boolean {
	let a = m.m22 * m.m33 - m.m32 * m.m23;
	let d = m.m32 * m.m13 - m.m12 * m.m33;
	let g = m.m12 * m.m23 - m.m22 * m.m13;

	let determinant = m.m11 * a + m.m21 * d + m.m31 * g;

	if (determinant === 0) {
		setToIdentity(target);
		return false;
	}

	let invertDeterminant = 1 / determinant;

	target.m11 = invertDeterminant * a;
	target.m21 = invertDeterminant * (m.m23 * m.m31 - m.m33 * m.m21);
	target.m31 = invertDeterminant * (m.m21 * m.m32 - m.m31 * m.m22);
	target.m12 = invertDeterminant * d;
	target.m22 = invertDeterminant * (m.m11 * m.m33 - m.m31 * m.m13);
	target.m32 = invertDeterminant * (m.m31 * m.m12 - m.m11 * m.m32);
	target.m13 = invertDeterminant * g;
	target.m23 = invertDeterminant * (m.m13 * m.m21 - m.m23 * m.m11);
	target.m33 = invertDeterminant * (m.m11 * m.m22 - m.m21 * m.m12);

	return true;
}