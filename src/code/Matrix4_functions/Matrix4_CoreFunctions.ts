import { Matrix4 } from "./Matrix4";

export function calcDeterminant(m: Matrix4): number {
	let a = (m.m33 * m.m44) - (m.m34 * m.m43);
	let b = (m.m32 * m.m44) - (m.m34 * m.m42);
	let c = (m.m32 * m.m43) - (m.m33 * m.m42);
	let d = (m.m31 * m.m44) - (m.m34 * m.m41);
	let e = (m.m31 * m.m43) - (m.m33 * m.m41);
	let f = (m.m31 * m.m42) - (m.m32 * m.m41);

	return (m.m11 * ((m.m22 * a) - (m.m23 * b) + (m.m24 * c))) -
		(m.m12 * ((m.m21 * a) - (m.m23 * d) + (m.m24 * e))) +
		(m.m13 * ((m.m21 * b) - (m.m22 * d) + (m.m24 * f))) -
		(m.m14 * ((m.m21 * c) - (m.m22 * e) + (m.m23 * f)));
}

export function areEquals(a: Matrix4, b: Matrix4): boolean {
	return a.m11 === b.m11 && a.m12 === b.m12 && a.m13 === b.m13 && a.m14 === b.m14 &&
		a.m21 === b.m21 && a.m22 === b.m22 && a.m23 === b.m23 && a.m24 === b.m24 &&
		a.m31 === b.m31 && a.m32 === b.m32 && a.m33 === b.m33 && a.m34 === b.m34 &&
		a.m41 === b.m41 && a.m42 === b.m42 && a.m43 === b.m43 && a.m44 === b.m44;
}

export function areCloseToEquals(a: Matrix4, b: Matrix4, tolerance?: number): boolean {
	if (!tolerance) tolerance = 1e-10;

	return Math.abs(a.m11 - b.m11) <= tolerance && Math.abs(a.m12 - b.m12) <= tolerance &&
		Math.abs(a.m13 - b.m13) <= tolerance && Math.abs(a.m14 - b.m14) <= tolerance &&
		Math.abs(a.m21 - b.m21) <= tolerance && Math.abs(a.m22 - b.m22) <= tolerance &&
		Math.abs(a.m23 - b.m23) <= tolerance && Math.abs(a.m24 - b.m24) <= tolerance &&
		Math.abs(a.m31 - b.m31) <= tolerance && Math.abs(a.m32 - b.m32) <= tolerance &&
		Math.abs(a.m33 - b.m33) <= tolerance && Math.abs(a.m34 - b.m34) <= tolerance &&
		Math.abs(a.m41 - b.m41) <= tolerance && Math.abs(a.m42 - b.m42) <= tolerance &&
		Math.abs(a.m43 - b.m43) <= tolerance && Math.abs(a.m44 - b.m44) <= tolerance;
}