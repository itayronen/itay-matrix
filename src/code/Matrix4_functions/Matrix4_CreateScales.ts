import { Matrix4 } from "./Matrix4";
import { create } from "./Matrix4_Create";

export function createScale(x: number, y: number, z: number): Matrix4 {
	return create(
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1
	);
}