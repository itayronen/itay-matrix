import { Matrix4 } from "./Matrix4";
import { create } from "./Matrix4_Create";

export function createRotationX(radians: number): Matrix4 {
	let cos = Math.cos(radians);
	let sin = Math.sin(radians);

	return create(
		1, 0.0, 0.0, 0,
		0, cos, -sin, 0,
		0, sin, cos, 0,
		0, 0.0, 0.0, 1
	);
}

export function createRotationY(radians: number): Matrix4 {
	let cos = Math.cos(radians);
	let sin = Math.sin(radians);

	return create(
		cos, 0, sin, 0,
		0, 1, 0, 0,
		-sin, 0, cos, 0,
		0, 0, 0, 1
	);
}

export function createRotationZ(radians: number): Matrix4 {
	let cos = Math.cos(radians);
	let sin = Math.sin(radians);

	return create(
		cos, -sin, 0, 0,
		sin, cos, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
}