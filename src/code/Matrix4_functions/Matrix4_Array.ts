import { Matrix4 } from "./Matrix4";

export interface WritableArrayLike<T> {
	readonly length: number;
	[n: number]: T;
}

export function setArrayFromMatrix(target: WritableArrayLike<number>, m: Matrix4, startIndex = 0): void {
	if (startIndex === 0) {
		target[0] = m.m11; target[4] = m.m12; target[8] = m.m13; target[12] = m.m14;
		target[1] = m.m21; target[5] = m.m22; target[9] = m.m23; target[13] = m.m24;
		target[2] = m.m31; target[6] = m.m32; target[10] = m.m33; target[14] = m.m34;
		target[3] = m.m41; target[7] = m.m42; target[11] = m.m43; target[15] = m.m44;
	}
	else {
		target[startIndex++] = m.m11;
		target[startIndex++] = m.m21;
		target[startIndex++] = m.m31;
		target[startIndex++] = m.m41;
		target[startIndex++] = m.m12;
		target[startIndex++] = m.m22;
		target[startIndex++] = m.m32;
		target[startIndex++] = m.m42;
		target[startIndex++] = m.m13;
		target[startIndex++] = m.m23;
		target[startIndex++] = m.m33;
		target[startIndex++] = m.m43;
		target[startIndex++] = m.m14;
		target[startIndex++] = m.m24;
		target[startIndex++] = m.m34;
		target[startIndex] = m.m44;
	}
}