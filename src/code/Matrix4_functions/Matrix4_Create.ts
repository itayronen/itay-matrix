import { Matrix4 } from "./Matrix4";

export function create(
	m11: number, m12: number, m13: number, m14: number,
	m21: number, m22: number, m23: number, m24: number,
	m31: number, m32: number, m33: number, m34: number,
	m41: number, m42: number, m43: number, m44: number,
): Matrix4 {
	return {
		m11, m12, m13, m14,
		m21, m22, m23, m24,
		m31, m32, m33, m34,
		m41, m42, m43, m44,
	};
}

export function createIdentity(): Matrix4 {
	return create(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
}

export function createClone(source: Matrix4): Matrix4 {
	return create(
		source.m11, source.m12, source.m13, source.m14,
		source.m21, source.m22, source.m23, source.m24,
		source.m31, source.m32, source.m33, source.m34,
		source.m41, source.m42, source.m43, source.m44,
	);
}