import { Matrix4 } from "./Matrix4";
import { create } from "./Matrix4_Create";

export function createTranslation(x: number, y: number, z: number): Matrix4 {
	return create(
		1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1
	);
}