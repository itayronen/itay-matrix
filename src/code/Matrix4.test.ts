import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Matrix4 } from "./Matrix4";

export default function (suite: TestSuite): void {
	suite.describe("Matrix4", suite => {
		suite.describe(Matrix4.areEquals, suite => {
			suite.test("When equals, returns true.", t => {
				t.arrange();
				let matrix1 = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
				let matrix2 = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

				t.act();
				let result = Matrix4.areEquals(matrix1, matrix2);

				t.assert();
				expect(result).to.equal(true);
			});
			
			suite.test("When not equals, returns false.", t => {
				t.arrange();
				let matrix1 = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
				let matrix2 = Matrix4.create(2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

				t.act();
				let result = Matrix4.areEquals(matrix1, matrix2);

				t.assert();
				expect(result).to.equal(false);
			});
		});

		suite.describe(Matrix4.areCloseToEquals, suite => {
			suite.test("When equals, returns true.", t => {
				t.arrange();
				let matrix1 = Matrix4.create(1.000000000008, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
				let matrix2 = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

				t.act();
				let result = Matrix4.areCloseToEquals(matrix1, matrix2);

				t.assert();
				expect(result).to.equal(true);
			});
			
			suite.test("When not equals, returns false.", t => {
				t.arrange();
				let matrix1 = Matrix4.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
				let matrix2 = Matrix4.create(2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

				t.act();
				let result = Matrix4.areCloseToEquals(matrix1, matrix2);

				t.assert();
				expect(result).to.equal(false);
			});
		});
	});
}