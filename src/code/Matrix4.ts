import * as Core from "./Matrix4_functions/Matrix4_CoreFunctions";
import * as Create from "./Matrix4_functions/Matrix4_Create";
import * as Translation from "./Matrix4_functions/Matrix4_CreateTranslations";
import * as Rotation from "./Matrix4_functions/Matrix4_CreateRotations";
import * as Scale from "./Matrix4_functions/Matrix4_CreateScales";
import * as ArrayFunc from "./Matrix4_functions/Matrix4_Array";

export interface Matrix4 {
	m11: number, m12: number, m13: number, m14: number,
	m21: number, m22: number, m23: number, m24: number,
	m31: number, m32: number, m33: number, m34: number,
	m41: number, m42: number, m43: number, m44: number,
}

export module Matrix4 {
	export const areCloseToEquals = Core.areCloseToEquals;
	export const areEquals = Core.areEquals;
	export const calcDeterminant = Core.calcDeterminant;
	export const create = Create.create;
	export const createClone = Create.createClone;
	export const createIdentity = Create.createIdentity;

	export const createRotationX = Rotation.createRotationX;
	export const createRotationY = Rotation.createRotationY;
	export const createRotationZ = Rotation.createRotationZ;

	export const createTranslation = Translation.createTranslation;

	export const createScale = Scale.createScale;

	export const setArrayFromMatrix = ArrayFunc.setArrayFromMatrix;
}