import { Matrix4 } from "./code/Matrix4";
import * as Matrix4Ref from "./code/Matrix4Ref";
import * as Matrix4Self from "./code/Matrix4Self";

export { Matrix4, Matrix4Ref, Matrix4Self }

import { Matrix3 } from "./code/Matrix3";
import * as Matrix3Ref from "./code/Matrix3Ref";

export { Matrix3, Matrix3Ref }
