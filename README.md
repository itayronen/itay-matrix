# itay-matrix
Flexible matrix library, written in typescript.

## Install
`npm install --save itay-matrix`

## Credits
The functions were rewritten based on:
* http://www.songho.ca/opengl/gl_matrix.html
* https://github.com/mrdoob/three.js
* https://en.wikipedia.org/wiki/Euler_angles